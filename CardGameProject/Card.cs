﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalCardGame
{
    enum CardSuit
    {
        Diamonds = 1,
        Hearts,
        Clubs,
        Spades
    }

    /// <summary>
    /// Represents a card in a card game
    /// </summary>
    class Card
    {
        /// <summary>
        /// The value of the card: 1 - 13
        /// </summary>
        private byte _value;

        /// <summary>
        /// The suit of the card
        /// </summary>
        private CardSuit _suit;

        public Card(byte value, CardSuit suit)
        {
            //initialize ALL field variables
            _value = value;
            _suit = suit;
        }


        /// <summary>
        /// Maximum card value allowed in a game for this type of card
        /// </summary>
        private const int MAX_CARD_VALUE = 13;

        /// <summary>
        /// Maximum suit count the cards support
        /// </summary>
        private const int MAX_SUIT_COUNT = 4;

        //Accessor and mutator methods with C# properties
        public byte Value
        {
            get { return _value; }
            set { _value = value; }
        }

        //create an accessor method for _suit
        public CardSuit Suit
        {
            get { return _suit; }
            set { _suit = value; }
        }

        public string CardName
        {
            get
            {
                //depending on the value of the card determine the proper card name (King = 13)
                string cardName;
                switch (_value)
                {
                    case 1:
                        cardName = "Ace";
                        break;

                    case 13:
                        cardName = "King";
                        break;

                    case 12:
                        cardName = "Queen";
                        break;

                    case 11:
                        cardName = "Jack";
                        break;

                    default:
                        cardName = _value.ToString();
                        break;
                }

                return cardName;
            }
        }
        
    }
}
