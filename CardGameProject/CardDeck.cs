﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalCardGame
{
    class CardDeck
    {
        private List<Card> _cardList;

        public CardDeck()
        {
            _cardList = new List<Card>();

            CreateCards();
        }

        private void CreateCards()
        {
            //repeat creating all the cards in each suit of the deck
            for (int iSuit = 0; iSuit < 4; iSuit++)
            {
                //create all the cards for the current suit
                CardSuit suit = (CardSuit)(iSuit + 1);

                //create the cards for the suit
                for (byte iCard = 1; iCard < 14; iCard++)
                {
                    //create the card
                    Card card = new Card(iCard, suit);

                    //add the card to the deck
                    _cardList.Add(card);
                }
            }
        }

        public void PrintDeck()
        {
            //go through all the cards in the deck and print them
            foreach (Card card in _cardList)
            {
                Debug.WriteLine($"{card.CardName} of {card.Value}");
            }
        }

        /// <summary>
        /// Obtain the number of cards in the deck. This is read-only property
        /// </summary>
        public int CardCount
        {
            get { return _cardList.Count; }
        }
    }
}
